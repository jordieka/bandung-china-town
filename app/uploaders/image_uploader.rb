class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :small do
      process resize_to_fit: [160,160]
  end

  version :thumb do
      process resize_to_fit: [320,320]
  end

  version :big do
      process resize_to_fit: [640,640]
  end

  def extension_whitelist
    %w(jpg jpeg png)
  end

  def default_url
      ''
  end
end
