module Api
  class ApplicationController < ActionController::API
    include Response
    # include ExceptionHandler
    # include ActionController::Serialization

    # before_action :authorize_request
    # before_action :prepare_exception_notifier
    # attr_reader :current_user

    # private
    #   # authorize request for api
    #   def authorize_request
    #     @current_user = (AuthorizeApiRequest.new(request.headers).call)[:customer]
    #   end

    #   def prepare_exception_notifier
    #     request.env["exception_notifier.exception_data"] = {
    #       current_user: current_user
    #     }
    #   end
  end
end
