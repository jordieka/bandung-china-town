class Api::V1::InformationController < Api::ApplicationController
  # skip_before_action :authorize_request, only: [:update_delivery_statuses]

  # index
  def index
    @infos = Information.all
    @infos = @infos.where(category: params[:category]) if params[:category]

    if @infos.nil?
      json_response({}, "Data not found!",:not_found)
    else
      json_response({data: @infos}, :ok)
    end
  end

end
