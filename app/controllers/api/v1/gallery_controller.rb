class Api::V1::GalleryController < Api::ApplicationController
  # skip_before_action :authorize_request, only: [:update_delivery_statuses]

  # index
  def index
    @gallery = Gallery.all

    if @gallery.nil?
    	json_response({}, "Data not found!",:not_found)
    else
    	json_response({data: @gallery}, :ok)
    end
  end

end
