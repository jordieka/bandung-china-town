class Api::V1::HistoryController < Api::ApplicationController
  # skip_before_action :authorize_request, only: [:update_delivery_statuses]

  # index
  def index
    @history = History.all

    if @history.nil?
    	json_response({}, "Data not found!",:not_found)
    else
    	json_response({data: @history}, :ok)
    end

  end

end
