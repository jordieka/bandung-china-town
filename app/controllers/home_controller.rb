class HomeController < ApplicationController

  # index of home controller
  def index
    render json: {
      status: true
    }
  end

end
