module Response

  STATUS_SUCCESS = 100..399

  # json response format for api
  def json_response(object, message = nil, status = :ok, page = nil)
    if page.nil? || page.empty?
      render json: { data: set_object(object), meta: response_meta(message, status) }, status: :ok
    else
      render json: { data: set_object(object), page: page, meta: response_meta(message, status) }, status: :ok
    end
  end

  # json response with serializer
  def json_response_with_serializer(object, page = {}, fields = nil, message = nil, status = :ok, **options)
    # options[:url] = root_url
    serialized_object = ActiveModelSerializers::SerializableResource.new(set_object(object), options)
    serialized_object.serialization_scope = current_user
    # serialized_object.serialization_scope_name = "current_user"
    serialized_object = serialized_object.as_json

    # serialized_object2 = ActiveModelSerializers::SerializableResource.new(set_object_page(page), options)
    # serialized_object2.serialization_scope = current_user
    # # serialized_object.serialization_scope_name = "current_user"
    # serialized_object2 = serialized_object2.as_json

    json_response(serialized_object, message , status, page)
  end

  # response fornat for meta
  def response_meta(message = nil, status = :ok)
    status_code = get_status_code(status)

    response = {
      "status": set_status_response(status_code),
      "code": status_code,
      "message": set_response_message(message, status_code)
    }

    return response
  end

  private

  def set_object(object)
    object = object || {}

    return object
  end

  def set_object_page(object, page = nil)
    object = object || {}
    object[:page] = {} if page.present?
    return object
  end

  def get_status_code(status)
    Rack::Utils::SYMBOL_TO_STATUS_CODE[status]
  end

  def set_response_message(message, status_code)
    return Rack::Utils::HTTP_STATUS_CODES[status_code] if message.nil?

    message
  end

  def set_status_response(status_code)
    STATUS_SUCCESS.include? status_code.to_i
  end
end
