module ExceptionHandler 
	extend ActiveSupport::Concern

	# define custom error subclasses, - rescue catches 'StandardErrors'
	class AuthenticationError < StandardError; end
	class MissingToken < StandardError; end
	class InvalidToken < StandardError; end
	# Custom exception
	class DuplicateRecord < StandardError; end
	class MissingAuthentication < StandardError; end
	class InvalidAuthentication < StandardError; end
	class ExpiredAuthentication < StandardError; end

	included do 

		# define custom handler 
		rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two
		rescue_from ExceptionHandler::AuthenticationError, with: :four_twenty_one
		rescue_from ExceptionHandler::MissingToken, with: :four_twenty_two
		rescue_from ExceptionHandler::InvalidToken, with: :four_twenty_two
		rescue_from ExceptionHandler::DuplicateRecord, with: :four_twenty_two
		rescue_from ExceptionHandler::MissingAuthentication, with: :four_twenty_two
		rescue_from ExceptionHandler::InvalidAuthentication, with: :bad_request_handler
		rescue_from ExceptionHandler::ExpiredAuthentication, with: :four_twenty_one

		rescue_from ActiveRecord::RecordNotFound do |e|
			json_response({}, e.message, :not_found)
		end

		private 
			# json response with message; status code 422 - unprocessable entity
			def four_twenty_two(e) 
				json_response({}, e.message, :unprocessable_entity)
			end

			# json response with message; status code  401 - unauthorized 
			def four_twenty_one(e)
				json_response({}, e.message, :unauthorized)
			end

			def bad_request_handler(e)
				json_response({}, e.message, :bad_request)
			end
	end
end