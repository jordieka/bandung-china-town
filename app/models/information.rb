class Information < ApplicationRecord
	enum category: [:kuliner, :fasilitas, :operasional]
	mount_uploader :image, ImageUploader
end
