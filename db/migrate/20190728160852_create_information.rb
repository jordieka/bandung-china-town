class CreateInformation < ActiveRecord::Migration[5.2]
  def change
    create_table :information do |t|
      t.integer :category
      t.string :name
      t.text :desc

      t.timestamps
    end

    add_index :information , :category
  end
end
